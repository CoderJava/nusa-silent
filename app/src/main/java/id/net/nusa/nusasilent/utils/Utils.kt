package id.net.nusa.nusasilent.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.provider.Settings
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.di.module.GlideApp
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.design.snackbar
import kotlin.reflect.KFunction0

const val permissionLocation = Manifest.permission.ACCESS_FINE_LOCATION
const val notificationChannelId = "nusa_silent_channel_id"
const val notificationChannelName = "nusasilent channel"
const val requestCodeSettings = 999

fun requestRuntimePermission(
    activity: Activity,
    permission: String,
    actionPermissionGranted: KFunction0<Unit>,
    actionPermissionDenied: KFunction0<Unit>,
    actionPermissionDeniedPermanent: KFunction0<Unit>
) {
    Dexter.withActivity(activity)
        .withPermission(permission)
        .withListener(object : PermissionListener {
            override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                actionPermissionGranted.invoke()
            }

            override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                if (response?.isPermanentlyDenied!!) {
                    actionPermissionDeniedPermanent.invoke()
                } else {
                    actionPermissionDenied.invoke()
                }
            }

            override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest?, token: PermissionToken?) {
                token?.continuePermissionRequest()
            }
        })
        .check()
}

fun showAlertDialogPermissionDeniedPermanent(context: Activity, message: String) {
    val alertDialogInfo = AlertDialog.Builder(context)
        .setTitle(context.getString(R.string.permission))
        .setMessage(context.getString(R.string.message_denied_permanent))
        .setCancelable(false)
        .setPositiveButton(context.getString(R.string.open_settings)) { dialog, _ ->
            dialog.dismiss()
            val intentSettingApplicationDetail = Intent()
            intentSettingApplicationDetail.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uriSettingApplicationDetail = Uri.fromParts("package", context.packageName, null)
            intentSettingApplicationDetail.data = uriSettingApplicationDetail
            context.startActivityForResult(intentSettingApplicationDetail, requestCodeSettings)
        }
        .setNegativeButton(context.getString(R.string.exit)) { dialog, _ ->
            dialog.dismiss()
            context.finish()
        }
        .create()
    alertDialogInfo.show()
}

fun showLongSnackbar(view: View, message: String) {
    view.longSnackbar(message)
}

fun showShortSnackbar(view: View, message: String) {
    view.snackbar(message)
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
    return if (connectivityManager is ConnectivityManager) {
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        networkInfo?.isConnected ?: false
    } else false
}

fun ImageView.setImageViewDrawableWithGlide(
    context: Context,
    drawable: Int? = null,
    source: String = "",
    isSavedCache: Boolean = false
) {
    GlideApp.with(context)
        .load(if (source.isEmpty()) drawable else source)
        .transition(DrawableTransitionOptions.withCrossFade())
        .diskCacheStrategy(if (isSavedCache) DiskCacheStrategy.ALL else DiskCacheStrategy.NONE)
        .into(this)
}

fun getBaseUrlPhotoReference(valuePhotoReference: String): String {
    return "https://maps.googleapis.com/maps/api/place/photo" +
            "?maxwidth=768" +
            "&photoreference=$valuePhotoReference" +
            "&key=AIzaSyABuuIwWUqVuD7IpEg7RyhqM4stcqt235E"
}

fun setColorLoadingSwipeRefreshLayout(context: Context, view: SwipeRefreshLayout) {
    view.setColorSchemeColors(
        ContextCompat.getColor(context, R.color.colorAccent),
        ContextCompat.getColor(context, R.color.colorPrimary)
    )
}

fun intentGoogleMapsDirection(context: Context, origin: String, destination: String) {
    val intentGoogleMapsDirection = Intent(
        Intent.ACTION_VIEW,
        Uri.parse("https://www.google.com/maps/dir/?api=1&origin=$origin&destination=$destination&travelmode=driving")
    )
    context.startActivity(intentGoogleMapsDirection)
}

fun getRingerModeDefault(context: Context): Int {
    val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    return audioManager.ringerMode
}

fun calculateDistanceLocation(latitudeDestination: Double, longitudeDestination: Double,
                              latitudeCurrentLocation: Double, longitudeCurrentLocation: Double): Double {
    val radiusEarth = 6371
    val dLat = deg2rad(latitudeCurrentLocation - latitudeDestination)
    val dLng = deg2rad(longitudeCurrentLocation - longitudeDestination)
    val sinLat = Math.sin(dLat / 2) * Math.sin(dLat / 2)
    val cos = Math.cos(deg2rad(latitudeDestination)) * Math.cos(deg2rad(latitudeCurrentLocation))
    val sinLng = Math.sin(dLng / 2) * Math.sin(dLng / 2)
    val a = sinLat + cos * sinLng
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    var d = radiusEarth * c
    d = ((Math.round(d * 100).toDouble()) / 1000)
    return d
}

fun deg2rad(degree: Double): Double {
    return degree * (Math.PI / 100)
}
