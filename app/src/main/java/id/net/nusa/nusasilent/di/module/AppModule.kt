package id.net.nusa.nusasilent.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ApiModule::class, OkHttpModule::class, RetrofitModule::class, SharedPreferencesModule::class, ViewModelModule::class, AppDatabaseModule::class])
class AppModule {

    @Provides
    @Singleton
    fun providesGson(): Gson = GsonBuilder().create()

}