package id.net.nusa.nusasilent.views.ui.fragments.dashboardfavorite.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.model.nearbyplaces.PlaceData
import id.net.nusa.nusasilent.utils.getBaseUrlPhotoReference
import id.net.nusa.nusasilent.utils.setImageViewDrawableWithGlide
import kotlinx.android.synthetic.main.item_place.view.*

class AdapterFavoritePlace(
    private val context: Context,
    private var placesData: MutableList<PlaceData>,
    private val listener: ListenerAdapterFavoritePlace
) : RecyclerView.Adapter<AdapterFavoritePlace.ViewHolderItemFavoritePlace>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderItemFavoritePlace {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_place, parent, false)
        return ViewHolderItemFavoritePlace(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolderItemFavoritePlace, position: Int) {
        val favoritePlace = placesData[position]
        holder.bind(favoritePlace)
    }

    override fun getItemCount(): Int = placesData.size

    fun refresh(placesData: MutableList<PlaceData>) {
        this.placesData = placesData
        notifyDataSetChanged()
    }

    inner class ViewHolderItemFavoritePlace constructor(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        fun bind(placeData: PlaceData) {
            itemView.image_view_favorite_item_place.setOnClickListener(this)
            itemView.image_view_direction_item_place.setOnClickListener(this)
            itemView.text_view_value_name_item_place.text = placeData.name
            itemView.text_view_value_address_item_place.text = placeData.address
            itemView.text_view_value_distance_item_place.text = context.getString(R.string.duration_value, placeData.distance, placeData.duration)
            if (placeData.photoCover != null && placeData.photoCover.isNotEmpty()) {
                val urlPhotoReference = getBaseUrlPhotoReference(placeData.photoCover)
                itemView.image_view_item_place.setImageViewDrawableWithGlide(
                    context,
                    source = urlPhotoReference,
                    isSavedCache = true
                )
            } else {
                itemView.image_view_item_place.setImageViewDrawableWithGlide(
                    context,
                    drawable = R.drawable.image_not_found
                )
            }
            if (placeData.isFavorite) {
                itemView.image_view_favorite_item_place.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_favorite_red_24dp
                    )
                )
            }
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.image_view_favorite_item_place -> {
                    listener.onClickRemoveItemFavorite(adapterPosition)
                }
                R.id.image_view_direction_item_place -> {
                    listener.onClickItemDirection(placesData[adapterPosition])
                }
            }
        }
    }

    interface ListenerAdapterFavoritePlace {

        fun onClickRemoveItemFavorite(adapterPosition: Int)

        fun onClickItemDirection(placeData: PlaceData)

    }

}