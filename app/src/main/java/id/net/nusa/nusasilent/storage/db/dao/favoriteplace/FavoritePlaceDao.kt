package id.net.nusa.nusasilent.storage.db.dao.favoriteplace

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import id.net.nusa.nusasilent.storage.db.entity.favoriteplace.FavoritePlaceEntity
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface FavoritePlaceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavoritePlace(favoritePlaceEntity: FavoritePlaceEntity)

    @Query("SELECT * FROM favorite_place")
    fun getAllFavoritePlaces(): Observable<List<FavoritePlaceEntity>>

    @Query("DELETE FROM favorite_place WHERE id = :id")
    fun deleteFavoritePlaceById(id: String)


}