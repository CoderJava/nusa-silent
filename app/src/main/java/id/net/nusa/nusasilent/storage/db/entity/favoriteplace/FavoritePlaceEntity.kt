package id.net.nusa.nusasilent.storage.db.entity.favoriteplace

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorite_place")
data class FavoritePlaceEntity (
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: String = "",
    @ColumnInfo(name = "id_place")
    var idPlace: String = "",
    @ColumnInfo(name = "latitude")
    var latitude: Double = 0.0,
    @ColumnInfo(name = "longitude")
    var longitude: Double = 0.0,
    @ColumnInfo(name = "name")
    var name: String = "",
    @ColumnInfo(name = "reference")
    var reference: String = "",
    @ColumnInfo(name = "type")
    var type: String = "",
    @ColumnInfo(name = "photo_cover")
    var photoCover: String = "",
    @ColumnInfo(name = "rating")
    val rating: Double = 0.0,
    @ColumnInfo(name = "user_rating_total")
    val userRatingTotal: Int = 0,
    @ColumnInfo(name = "address")
    val address: String = "",
    @ColumnInfo(name = "distance")
    val distance: String = "",
    @ColumnInfo(name = "duration")
    val duration: String = ""
)