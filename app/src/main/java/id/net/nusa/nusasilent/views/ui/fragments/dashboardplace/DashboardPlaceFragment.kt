package id.net.nusa.nusasilent.views.ui.fragments.dashboardplace


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.di.injectable.Injectable
import id.net.nusa.nusasilent.model.nearbyplaces.PlaceData
import id.net.nusa.nusasilent.utils.*
import id.net.nusa.nusasilent.views.ui.fragments.dashboardplace.adapter.AdapterNearbyPlace
import kotlinx.android.synthetic.main.fragment_dashboard_place.view.*
import javax.inject.Inject

class DashboardPlaceFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var rootView: View
    private lateinit var dashboardPlaceViewModel: DashboardPlaceViewModel
    private lateinit var placesData: MutableList<PlaceData>
    private lateinit var adapterNearbyPlace: AdapterNearbyPlace

    companion object {
        fun newInstance(): DashboardPlaceFragment = DashboardPlaceFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_dashboard_place, container, false)
        initAdapterNearbyPlace()
        return rootView
    }

    private fun initAdapterNearbyPlace() {
        placesData = mutableListOf()
        adapterNearbyPlace =
            AdapterNearbyPlace(context!!, placesData, object : AdapterNearbyPlace.ListenerAdapterNearbyPlace {
                override fun onClickItemFavorite(adapterPosition: Int) {
                    dashboardPlaceViewModel.onUpdateItemFavorite(adapterPosition)
                }

                @SuppressLint("MissingPermission")
                override fun onClickItemDirection(placeData: PlaceData) {
                    val client = LocationServices.getFusedLocationProviderClient(context!!)
                    client.lastLocation
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                val currentLocation = it.result
                                val latitudeCurrentLocation = currentLocation?.latitude
                                val longitudeCurrentLocation = currentLocation?.longitude
                                val latLngCurrentLocation =
                                    LatLng(latitudeCurrentLocation!!, longitudeCurrentLocation!!)
                                val strLatLngCurrentLocation =
                                    "${latLngCurrentLocation.latitude},${latLngCurrentLocation.longitude}"
                                val strLatLngDestinationLocation =
                                    "${placeData.latitude},${placeData.longitude}"
                                intentGoogleMapsDirection(
                                    context!!,
                                    strLatLngCurrentLocation,
                                    strLatLngDestinationLocation
                                )
                            } else {
                                showLongSnackbar(rootView, it.exception?.message!!)
                            }
                        }
                }
            })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
        observeViewModel()
        doRequestRuntimePermissionLocation()
    }

    private fun initViews() {
        setColorLoadingSwipeRefreshLayout(context!!, rootView.swipe_refresh_layout_fragment_dashboard_place)
    }

    override fun onDestroy() {
        dashboardPlaceViewModel.dispose()
        super.onDestroy()
    }

    private fun initListeners() {
        rootView.swipe_refresh_layout_fragment_dashboard_place.setOnRefreshListener {
            doRefreshData()
        }
    }

    private fun observeViewModel() {
        dashboardPlaceViewModel = ViewModelProviders.of(this, viewModelFactory).get(DashboardPlaceViewModel::class.java)
        dashboardPlaceViewModel.error.observe(this, Observer<String> {
            if (it.isEmpty()) {
                rootView.recycler_view_fragment_dashboard_place.visible()
            } else {
                rootView.recycler_view_fragment_dashboard_place.gone()
                showLongSnackbar(rootView, it)
            }
        })
        dashboardPlaceViewModel.messageItem.observe(this, Observer<String> {
            if (it.isNotEmpty()) {
                showLongSnackbar(rootView, it)
            }
        })
        dashboardPlaceViewModel.loading.observe(this, Observer<Boolean> {
            rootView.swipe_refresh_layout_fragment_dashboard_place.isRefreshing = it
        })
        dashboardPlaceViewModel.nearbyPlacesData.observe(this, Observer<List<PlaceData>> {
            placesData.clear()
            placesData.addAll(it)
            if (rootView.recycler_view_fragment_dashboard_place.adapter == null) {
                rootView.recycler_view_fragment_dashboard_place.layoutManager = LinearLayoutManager(context)
                rootView.recycler_view_fragment_dashboard_place.adapter = adapterNearbyPlace
            } else {
                adapterNearbyPlace.refresh(placesData)
            }
        })
    }

    @SuppressLint("MissingPermission")
    private fun doLoadData() {
        val client = LocationServices.getFusedLocationProviderClient(context!!)
        client.lastLocation
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val currentLocation = it.result
                    val latitudeCurrentLocation = currentLocation?.latitude
                    val longitudeCurrentLocation = currentLocation?.longitude
                    val latLngCurrentLocation = LatLng(latitudeCurrentLocation!!, longitudeCurrentLocation!!)
                    dashboardPlaceViewModel.onLoadData(latLngCurrentLocation)
                } else {
                    showLongSnackbar(rootView, it.exception?.message!!)
                }
            }
    }

    @SuppressLint("MissingPermission")
    private fun doRefreshData() {
        val client = LocationServices.getFusedLocationProviderClient(context!!)
        client.lastLocation
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val currentLocation = it.result
                    val latitudeCurrentLocation = currentLocation?.latitude
                    val longitudeCurrentLocation = currentLocation?.longitude
                    val latLngCurrentLocation = LatLng(latitudeCurrentLocation!!, longitudeCurrentLocation!!)
                    dashboardPlaceViewModel.onRefreshData(latLngCurrentLocation)
                } else {
                    showLongSnackbar(rootView, it.exception?.message!!)
                }
            }
    }

    private fun doRequestRuntimePermissionLocation() {
        requestRuntimePermission(
            activity!!,
            permissionLocation,
            this::actionPermissionLocationGranted,
            this::actionPermissionLocationDenied,
            this::actionPermissionLocationDeniedPermanent
        )
    }

    private fun actionPermissionLocationGranted() {
        doLoadData()
    }

    private fun actionPermissionLocationDenied() {
        val alertDialogInfoPermissionDenied = AlertDialog.Builder(context!!)
            .setTitle(getString(R.string.permission))
            .setMessage(getString(R.string.message_denied_location_permission))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.request_again)) { dialog, _ ->
                dialog.dismiss()
                doRequestRuntimePermissionLocation()
            }
            .setNegativeButton(getString(R.string.exit)) { dialog, _ ->
                dialog.dismiss()
                activity?.finish()
            }
            .create()
        alertDialogInfoPermissionDenied.show()
    }

    private fun actionPermissionLocationDeniedPermanent() {
        showAlertDialogPermissionDeniedPermanent(activity!!, getString(R.string.message_denied_permanent))
    }

}
