package id.net.nusa.nusasilent.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.net.nusa.nusasilent.views.ui.activities.dashboard.DashboardActivity
import id.net.nusa.nusasilent.views.ui.activities.main.MainActivity

@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeDashboardActivity(): DashboardActivity

}