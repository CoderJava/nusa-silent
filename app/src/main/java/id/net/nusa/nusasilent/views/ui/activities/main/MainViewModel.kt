package id.net.nusa.nusasilent.views.ui.activities.main

import android.app.Application
import android.content.Context
import android.media.AudioManager
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import id.net.nusa.nusasilent.storage.sharedpreferences.SharedPreferencesManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val context: Application,
    private val sharedPreferencesManager: SharedPreferencesManager
) : AndroidViewModel(context) {

    val timer = MutableLiveData<Boolean>()
    val compositeDisposable = CompositeDisposable()

    fun onRunTimer() {
        val disposableRunTiemr = Observable.just(true)
            .delay(3, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<Boolean>() {
                override fun onNext(t: Boolean) {
                    val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
                    if (!sharedPreferencesManager.hasKey(SharedPreferencesManager.isOnMosque)) {
                        sharedPreferencesManager.putDataBoolean(SharedPreferencesManager.isOnMosque, false)
                    }
                    timer.value = t
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }

                override fun onComplete() {
                    /* Nothing to do in here */
                }
            })
        compositeDisposable.add(disposableRunTiemr)
    }

    fun dispose() {
        compositeDisposable.dispose()
    }

}