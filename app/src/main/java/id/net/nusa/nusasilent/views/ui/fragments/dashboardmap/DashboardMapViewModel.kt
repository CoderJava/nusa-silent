package id.net.nusa.nusasilent.views.ui.fragments.dashboardmap

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.api.MainApi
import id.net.nusa.nusasilent.model.nearbyplaces.NearbyPlacesResponse
import id.net.nusa.nusasilent.model.nearbyplaces.PlaceData
import id.net.nusa.nusasilent.storage.sharedpreferences.SharedPreferencesManager
import id.net.nusa.nusasilent.utils.getRingerModeDefault
import id.net.nusa.nusasilent.utils.isNetworkAvailable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DashboardMapViewModel @Inject constructor(
    private val context: Application,
    private val mainApi: MainApi,
    private val sharedPreferencesManager: SharedPreferencesManager
) : AndroidViewModel(context) {

    private val compositeDisposable = CompositeDisposable()
    val loading = MutableLiveData<Boolean>()
    val error = MutableLiveData<String>()
    val nearbyPlaces = MutableLiveData<List<PlaceData>>()
    val activityRecognition = MutableLiveData<String>()
    val ringerMode = MutableLiveData<Int>()

    fun onGetNearbyPlaces(latLngCurrentLocation: LatLng) {
        loading.value = true
        if (!isNetworkAvailable(context)) {
            loading.value = false
            error.value = context.getString(R.string.a_connection_error_please_try_again)
            return
        }
        val strLocation = "${latLngCurrentLocation.latitude},${latLngCurrentLocation.longitude}"
        val disposableNearbyPlaces = mainApi.getNearbyPlaces(strLocation)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<NearbyPlacesResponse>() {
                override fun onNext(t: NearbyPlacesResponse) {
                    loading.postValue(false)
                    error.postValue("")
                    val nearbyPlacesTemp = mutableListOf<PlaceData>()
                    t.results.forEach {
                        val placeData = PlaceData(
                            it.id,
                            it.place_id,
                            it.geometry.location.lat,
                            it.geometry.location.lng,
                            it.name,
                            it.reference,
                            it.types.toString(),
                            if (it.photos != null && it.photos.size > 0) it.photos[0].photo_reference else "",
                            false,
                            it.rating,
                            it.user_ratings_total
                        )
                        nearbyPlacesTemp.add(placeData)
                    }
                    nearbyPlaces.postValue(nearbyPlacesTemp)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    loading.postValue(false)
                    error.postValue(e.message)
                    nearbyPlaces.postValue(mutableListOf())
                }

                override fun onComplete() {
                    /* Nothing to do in here */
                }
            })
        compositeDisposable.add(disposableNearbyPlaces)
    }

    fun onGetLatestActivityRecognition() {
        val latestActivityRecognition = sharedPreferencesManager.getDataString(
            SharedPreferencesManager.activityRecognition,
            context.getString(R.string.still)
        )
        activityRecognition.postValue(latestActivityRecognition)
    }

    fun onGetRingerMode() {
        val ringerModeTemp = getRingerModeDefault(context)
        ringerMode.postValue(ringerModeTemp)
    }

    fun dispose() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

}