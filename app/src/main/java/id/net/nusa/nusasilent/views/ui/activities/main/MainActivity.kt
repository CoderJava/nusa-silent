package id.net.nusa.nusasilent.views.ui.activities.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.utils.permissionLocation
import id.net.nusa.nusasilent.utils.requestCodeSettings
import id.net.nusa.nusasilent.utils.requestRuntimePermission
import id.net.nusa.nusasilent.utils.showAlertDialogPermissionDeniedPermanent
import id.net.nusa.nusasilent.views.base.BaseActivity
import id.net.nusa.nusasilent.views.ui.activities.dashboard.DashboardActivity
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewModel()
        observeViewModel()
        doRequestRuntimePermissionLocation()
    }

    private fun observeViewModel() {
        mainViewModel.timer.observe(this, Observer<Boolean> {
            startActivity(intentFor<DashboardActivity>().clearTask().newTask())
        })
    }

    private fun initViewModel() {
        mainViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MainViewModel::class.java)
    }

    private fun doRequestRuntimePermissionLocation() {
        requestRuntimePermission(
            this,
            permissionLocation,
            this::actionPermissionLocationGranted,
            this::actionPermissionLocationDenied,
            this::actionPermissionLocationDeniedPermanent
        )
    }

    override fun onDestroy() {
        mainViewModel.dispose()
        super.onDestroy()
    }

    private fun actionPermissionLocationGranted() {
        mainViewModel.onRunTimer()
    }

    private fun actionPermissionLocationDenied() {
        val alertDialogInfoPermissionDenied = AlertDialog.Builder(this)
            .setTitle(getString(R.string.permission))
            .setMessage(getString(R.string.message_denied_location_permission))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.request_again)) { dialog, _ ->
                dialog.dismiss()
                doRequestRuntimePermissionLocation()
            }
            .setNegativeButton(getString(R.string.exit)) { dialog, _ ->
                dialog.dismiss()
                finish()
            }
            .create()
        alertDialogInfoPermissionDenied.show()
    }

    private fun actionPermissionLocationDeniedPermanent() {
        showAlertDialogPermissionDeniedPermanent(this, getString(R.string.message_denied_permanent))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            requestCodeSettings -> {
                doRequestRuntimePermissionLocation()
            }
        }
    }

}
