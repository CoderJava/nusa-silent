package id.net.nusa.nusasilent.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import id.net.nusa.nusasilent.di.key.ViewModelKey
import id.net.nusa.nusasilent.di.viewmodelfactory.CustomViewModelFactory
import id.net.nusa.nusasilent.views.ui.activities.main.MainViewModel
import id.net.nusa.nusasilent.views.ui.fragments.dashboardfavorite.DashboardFavoriteViewModel
import id.net.nusa.nusasilent.views.ui.fragments.dashboardmap.DashboardMapViewModel
import id.net.nusa.nusasilent.views.ui.fragments.dashboardplace.DashboardPlaceViewModel

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardMapViewModel::class)
    abstract fun bindDashboardMapViewModel(dashboardMapViewModel: DashboardMapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardPlaceViewModel::class)
    abstract fun bindDashboardPlaceViewModel(dashboardPlaceViewModel: DashboardPlaceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardFavoriteViewModel::class)
    abstract fun bindDashboardFavoriteViewModel(dashboardFavoriteViewModel: DashboardFavoriteViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: CustomViewModelFactory): ViewModelProvider.Factory

}