package id.net.nusa.nusasilent.api

import id.net.nusa.nusasilent.BuildConfig
import id.net.nusa.nusasilent.model.distanceplaces.DistanceByTwoPlacesResponse
import id.net.nusa.nusasilent.model.nearbyplaces.NearbyPlacesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MainApi {

    @GET("maps/api/place/nearbysearch/json")
    fun getNearbyPlaces(
        @Query("location") location: String, @Query("radius") radius: Int = 1000,
        @Query("sensor") sensor: Boolean = true, @Query("types") types: String = "mosque",
        @Query("key") key: String = BuildConfig.API_KEY_MAP
    ): Observable<NearbyPlacesResponse>

    @GET("maps/api/distancematrix/json")
    fun getDistanceByTwoPlaces(
        @Query("units") units: String = "metric",
        @Query("origins") origins: String,
        @Query("destinations") destinations: String,
        @Query("key") key: String = BuildConfig.API_KEY_MAP
    ): Observable<DistanceByTwoPlacesResponse>

}