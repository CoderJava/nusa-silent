package id.net.nusa.nusasilent.views.ui.activities.dashboard

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.google.android.gms.location.ActivityRecognitionClient
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.utils.getRingerModeDefault
import id.net.nusa.nusasilent.utils.notificationChannelId
import id.net.nusa.nusasilent.utils.notificationChannelName
import id.net.nusa.nusasilent.views.base.BaseActivity
import id.net.nusa.nusasilent.views.services.recognitionactivity.ConstantsRecognitionActivity
import id.net.nusa.nusasilent.views.services.recognitionactivity.RecognitionActivitiesIntentService
import id.net.nusa.nusasilent.views.services.workmanager.AppReceiver
import id.net.nusa.nusasilent.views.ui.fragments.dashboardfavorite.DashboardFavoriteFragment
import id.net.nusa.nusasilent.views.ui.fragments.dashboardmap.DashboardMapFragment
import id.net.nusa.nusasilent.views.ui.fragments.dashboardplace.DashboardPlaceFragment
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import views.services.workmanager.LocationWorkManager
import java.util.*
import java.util.concurrent.TimeUnit

class DashboardActivity : BaseActivity() {

    private lateinit var activityRecognitionClient: ActivityRecognitionClient
    private lateinit var pendingIntent: PendingIntent
    private val alarmRequestCode = 134
    private val intervalSeconds = 10
    private val notificationId = 1
    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        initListeners()
        setSelectedNavDefault()
        requestActivityRecognition()
        val disposableStartAlarmManager = Observable.just(true)
            .delay(5, TimeUnit.SECONDS)
            .subscribeWith(object : DisposableObserver<Boolean>() {
                override fun onNext(t: Boolean) {
                    /* Nothing to do in here */
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }

                override fun onComplete() {
                    startAlarmManager()
                }
            })
        compositeDisposable.add(disposableStartAlarmManager)
    }

    private fun startAlarmManager() {
        val alarmIntent = Intent(this, AppReceiver::class.java)
        pendingIntent = PendingIntent.getBroadcast(this, alarmRequestCode, alarmIntent, 0)
        val cal = Calendar.getInstance()
        cal.add(Calendar.SECOND, intervalSeconds)
        val manager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        manager.set(AlarmManager.RTC_WAKEUP, cal.timeInMillis, pendingIntent)
        createNotification()
    }

    private fun createNotification() {
        val titleNotification = "Nusa Silent"
        var contentNotification = getString(R.string.ringer) + ": "
        val ringerMode = getRingerModeDefault(this)
        contentNotification += when (ringerMode) {
            AudioManager.RINGER_MODE_NORMAL -> {
                getString(R.string.normal)
            }
            AudioManager.RINGER_MODE_SILENT, AudioManager.RINGER_MODE_VIBRATE -> {
                getString(R.string.silent)
            }
            else -> getString(R.string.unknown)
        }
        val alarmNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intentDashboardActivity = Intent(this, DashboardActivity::class.java)
        val pendingIntentNotification =
            PendingIntent.getActivity(this, 0, intentDashboardActivity, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(notificationChannelId, notificationChannelName, importance)
            alarmNotificationManager.createNotificationChannel(mChannel)
        }

        val alarmNotificationBuilder = NotificationCompat.Builder(this, notificationChannelId)
        alarmNotificationBuilder.setContentTitle(titleNotification)
        alarmNotificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
        alarmNotificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        alarmNotificationBuilder.setContentText(contentNotification)
        alarmNotificationBuilder.setAutoCancel(true)
        alarmNotificationBuilder.setContentIntent(pendingIntentNotification)
        alarmNotificationBuilder.setOngoing(true)
        alarmNotificationManager.notify(notificationId, alarmNotificationBuilder.build())
        val locationWorkManager = OneTimeWorkRequest.Builder(LocationWorkManager::class.java)
            .build()
        WorkManager.getInstance(this).enqueue(locationWorkManager)
    }

    private fun stopAlarmManager() {
        val manager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        manager.cancel(pendingIntent)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(notificationId)
        toast("Alarm Manager stopped by user")
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        removeActivityRecognition()
        super.onDestroy()
    }

    private fun initListeners() {
        bottom_navigation_view_activity_dashboard.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_navigation_item_place -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frame_layout_container_activity_dashboard, DashboardPlaceFragment.newInstance())
                        .commit()
                    true
                }
                R.id.menu_navigation_item_map -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frame_layout_container_activity_dashboard, DashboardMapFragment.newInstance())
                        .commit()
                    true
                }
                R.id.menu_navigation_item_favorite -> {
                    supportFragmentManager.beginTransaction()
                        .replace(
                            R.id.frame_layout_container_activity_dashboard,
                            DashboardFavoriteFragment.newInstance()
                        )
                        .commit()
                    true
                }
                else -> false
            }
        }
    }

    private fun setSelectedNavDefault() {
        bottom_navigation_view_activity_dashboard.selectedItemId = R.id.menu_navigation_item_map
    }

    @SuppressLint("MissingPermission")
    private fun requestActivityRecognition() {
        activityRecognitionClient = ActivityRecognitionClient(this)
        val task = activityRecognitionClient.requestActivityUpdates(
            ConstantsRecognitionActivity.detectionIntervalInMilliseconds,
            getActivityRecognitionPendingIntent()
        )
        task
            .addOnSuccessListener {
                AnkoLogger<DashboardActivity>().info { "request activity recognition success" }
            }
            .addOnFailureListener {
                it.printStackTrace()
            }
    }

    @SuppressLint("MissingPermission")
    private fun removeActivityRecognition() {
        val task = activityRecognitionClient.removeActivityUpdates(getActivityRecognitionPendingIntent())
        task
            .addOnSuccessListener {
                AnkoLogger<DashboardActivity>().info { "remove activity recognition success" }
            }
            .addOnFailureListener {
                it.printStackTrace()
            }
    }

    private fun getActivityRecognitionPendingIntent(): PendingIntent {
        val intentActivityRecognition = intentFor<RecognitionActivitiesIntentService>()
        return PendingIntent.getService(this, 0, intentActivityRecognition, PendingIntent.FLAG_UPDATE_CURRENT)
    }
}
