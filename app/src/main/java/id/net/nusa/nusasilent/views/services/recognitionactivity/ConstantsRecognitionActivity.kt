package id.net.nusa.nusasilent.views.services.recognitionactivity

import com.google.android.gms.location.DetectedActivity

class ConstantsRecognitionActivity {

    companion object {
        val packageName = "com.google.android.gms.location.actiivtyrecognition"
        val keyActivityUpdateRequested = "$packageName.ACTIVITY_UPDATE_REQUESTED"
        val keyDetectedActivities = "$packageName.DETECTED_ACTIVITIES"
        val detectionIntervalInMilliseconds = 30 * 1000L
        val monitoredActivities = arrayOf(
            DetectedActivity.STILL,
            DetectedActivity.ON_FOOT,
            DetectedActivity.WALKING,
            DetectedActivity.RUNNING,
            DetectedActivity.ON_BICYCLE,
            DetectedActivity.IN_VEHICLE,
            DetectedActivity.TILTING,
            DetectedActivity.UNKNOWN
        )
    }

}