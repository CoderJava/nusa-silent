package id.net.nusa.nusasilent.storage.db.repository.favoriteplace

import id.net.nusa.nusasilent.storage.db.dao.favoriteplace.FavoritePlaceDao
import id.net.nusa.nusasilent.storage.db.entity.favoriteplace.FavoritePlaceEntity
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class FavoritePlaceRepository @Inject constructor(private val favoritePlaceDao: FavoritePlaceDao) {

    fun getAllFavoritePlaces(): Observable<List<FavoritePlaceEntity>> = favoritePlaceDao.getAllFavoritePlaces()

    fun insertFavoritePlace(favoritePlaceEntity: FavoritePlaceEntity) =
        favoritePlaceDao.insertFavoritePlace(favoritePlaceEntity)

    fun deleteFavoritePlaceById(id: String) = favoritePlaceDao.deleteFavoritePlaceById(id)

}