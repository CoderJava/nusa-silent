package id.net.nusa.nusasilent.di.injectable

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import id.net.nusa.nusasilent.views.base.App

class AppInjector {
    
    companion object {
        fun init(app: App) {
            app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
                override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                    handleActivity(activity!!)
                }

                override fun onActivityStarted(activity: Activity?) {
                    /* Nothing to do in here */
                }

                override fun onActivityResumed(activity: Activity?) {
                    /* Nothing to do in here */
                }

                override fun onActivityPaused(activity: Activity?) {
                    /* Nothing to do in here */
                }

                override fun onActivityStopped(activity: Activity?) {
                    /* Nothing to do in here */
                }

                override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
                    /* Nothing to do in here */
                }

                override fun onActivityDestroyed(activity: Activity?) {
                    /* Nothing to do in here */
                }
            })
        }

        private fun handleActivity(activity: Activity) {
            if (activity is HasSupportFragmentInjector) {
                AndroidInjection.inject(activity)
            }
            if (activity is FragmentActivity) {
                activity.supportFragmentManager
                    .registerFragmentLifecycleCallbacks(object : FragmentManager.FragmentLifecycleCallbacks() {
                        override fun onFragmentCreated(fm: FragmentManager, fragment: Fragment, savedInstanceState: Bundle?) {
                            if (fragment is Injectable) {
                                AndroidSupportInjection.inject(fragment)
                            }
                        }
                    }, true)
            }
        }
    }
    
}