package id.net.nusa.nusasilent.di.module

import android.app.Application
import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [SharedPreferencesModule::class])
class OkHttpModule {

    private fun getBaseBuilder(cache: Cache): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .cache(cache)
            .retryOnConnectionFailure(true)
            .connectTimeout(3, TimeUnit.MINUTES)
            .readTimeout(3, TimeUnit.MINUTES)
            .writeTimeout(3, TimeUnit.MINUTES)
    }

    class CachingControlInterceptor : Interceptor {

        override fun intercept(chain: Interceptor.Chain): Response {
            val requestBuilder = chain.request().newBuilder()
            val cacheControl = CacheControl.Builder()
                .maxStale(1, TimeUnit.MINUTES)
                .maxAge(1, TimeUnit.MINUTES)
                .build()
            requestBuilder.cacheControl(cacheControl)
            val request = requestBuilder.build()
            val originalResponse = chain.proceed(request)
            return originalResponse.newBuilder()
                .removeHeader("Pragma")
                .removeHeader("Cache-Control")
                .header("Cache-Control", "public, only-if-cached, max-stale=604800")
                .build()
        }

    }

    @Provides
    @Singleton
    fun providesOkHttpClient(application: Application): Cache = Cache(application.cacheDir, 10 * 1024 * 1024)

    @Provides
    @Singleton
    fun providesLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BASIC
        return interceptor
    }

    @Provides
    @Singleton
    fun providesCachingControlInterceptor(): CachingControlInterceptor = CachingControlInterceptor()

    @Provides
    @Singleton
    fun providesOkHttp(cache: Cache, loggingInterceptor: HttpLoggingInterceptor, cachingControlInterceptor: CachingControlInterceptor): OkHttpClient {
        return getBaseBuilder(cache)
            .addNetworkInterceptor(cachingControlInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()
    }


}