package id.net.nusa.nusasilent.views.ui.fragments.dashboardplace

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.api.MainApi
import id.net.nusa.nusasilent.model.distanceplaces.DistanceByTwoPlacesResponse
import id.net.nusa.nusasilent.model.nearbyplaces.NearbyPlacesResponse
import id.net.nusa.nusasilent.model.nearbyplaces.PlaceData
import id.net.nusa.nusasilent.storage.db.entity.favoriteplace.FavoritePlaceEntity
import id.net.nusa.nusasilent.storage.db.repository.favoriteplace.FavoritePlaceRepository
import id.net.nusa.nusasilent.utils.isNetworkAvailable
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.Observable
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import javax.inject.Inject

class DashboardPlaceViewModel @Inject constructor(
    private val context: Application,
    private val mainApi: MainApi,
    private val favoritePlaceRepository: FavoritePlaceRepository
) : AndroidViewModel(context) {

    val loading = MutableLiveData<Boolean>()
    val error = MutableLiveData<String>()
    val messageItem = MutableLiveData<String>()
    val nearbyPlacesData = MutableLiveData<List<PlaceData>>()
    private val compositeDisposable = CompositeDisposable()

    fun onLoadData(location: LatLng) {
        AnkoLogger<DashboardPlaceViewModel>().info { "onLoadData location: $location" }
        loading.value = true
        error.value = ""
        if (!isNetworkAvailable(context)) {
            loading.value = false
            error.value = context.getString(R.string.a_connection_error_please_try_again)
            nearbyPlacesData.value = mutableListOf()
            return
        }
        val strLocation = "${location.latitude},${location.longitude}"
        val disposableNearbyPlace = mainApi.getNearbyPlaces(strLocation)
        val disposableFavoritePlace = favoritePlaceRepository.getAllFavoritePlaces()
        val disposableLoadData = Observable.zip(
            disposableNearbyPlace,
            disposableFavoritePlace,
            BiFunction<NearbyPlacesResponse, List<FavoritePlaceEntity>, List<PlaceData>> { t1, t2 ->
                val nearbyPlacesDataTemp = mutableListOf<PlaceData>()
                t1.results.forEach { result ->
                    val itemNearbyPlaceData = PlaceData(
                        result.id,
                        result.place_id,
                        result.geometry.location.lat,
                        result.geometry.location.lng,
                        result.name,
                        result.reference,
                        result.types.toString(),
                        if (result.photos != null && result.photos.size > 0) result.photos[0].photo_reference else "",
                        false,
                        result.rating,
                        result.user_ratings_total
                    )
                    for (favorite in t2) {
                        if (favorite.id == itemNearbyPlaceData.id) {
                            itemNearbyPlaceData.isFavorite = true
                            break
                        }
                    }
                    nearbyPlacesDataTemp.add(itemNearbyPlaceData)
                }
                nearbyPlacesDataTemp
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<List<PlaceData>>() {
                override fun onNext(t: List<PlaceData>) {
                    onGetDistanceNearbyPlaces(location, t)
                    /*loading.postValue(false)
                    error.postValue("")
                    nearbyPlacesData.postValue(t)*/
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    loading.postValue(false)
                    error.postValue(e.message)
                    nearbyPlacesData.postValue(mutableListOf())
                }

                override fun onComplete() {
                    /* Nothing to do in here */
                }
            })
        compositeDisposable.add(disposableLoadData)
    }

    private fun onGetDistanceNearbyPlaces(originsLocation: LatLng, placesData: List<PlaceData>) {
        if (!isNetworkAvailable(context)) {
            loading.value = false
            error.value = context.getString(R.string.a_connection_error_please_try_again)
            nearbyPlacesData.value = mutableListOf()
            return
        }
        val origins = "${originsLocation.latitude},${originsLocation.longitude}"
        val disposableGetDistanceNearbyPlaces = Observable
            .fromIterable(placesData)
            .flatMap {
                val destinations = "${it.latitude},${it.longitude}"
                return@flatMap mainApi.getDistanceByTwoPlaces(origins = origins, destinations = destinations)
            }
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : SingleObserver<MutableList<DistanceByTwoPlacesResponse>> {
                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onSuccess(t: MutableList<DistanceByTwoPlacesResponse>) {
                    val placesDataTemp = mutableListOf<PlaceData>()
                    for (a in 0 until t.size) {
                        val itemPlaceData = placesData[a]
                        val distanceByTwoPlacesResponse = t[a]
                        if (distanceByTwoPlacesResponse.rows != null && distanceByTwoPlacesResponse.rows.size > 0) {
                            itemPlaceData.address =
                                if (distanceByTwoPlacesResponse.destination_addresses != null
                                    && distanceByTwoPlacesResponse.destination_addresses.size > 0
                                ) distanceByTwoPlacesResponse.destination_addresses[0]
                                else ""
                            itemPlaceData.distance = distanceByTwoPlacesResponse.rows[0].elements[0].distance.text
                            itemPlaceData.duration = distanceByTwoPlacesResponse.rows[0].elements[0].duration.text
                        }
                        placesDataTemp.add(itemPlaceData)
                        placesDataTemp.sortBy { it.distance }
                    }
                    loading.postValue(false)
                    error.postValue("")
                    nearbyPlacesData.postValue(placesDataTemp)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    loading.postValue(false)
                    error.postValue(e.message)
                    nearbyPlacesData.postValue(mutableListOf())
                }
            })

    }

    fun dispose() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    fun onRefreshData(location: LatLng) {
        loading.value = true
        error.value = ""
        if (!isNetworkAvailable(context)) {
            loading.value = false
            error.value = context.getString(R.string.a_connection_error_please_try_again)
            nearbyPlacesData.value = mutableListOf()
            return
        }
        val strLocation = "${location.latitude},${location.longitude}"
        val disposableRefreshData = mainApi.getNearbyPlaces(strLocation)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<NearbyPlacesResponse>() {
                override fun onNext(t: NearbyPlacesResponse) {
                    loading.postValue(false)
                    error.postValue("")
                    /*nearbyPlacesData.postValue(t.results)*/
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    loading.postValue(false)
                    error.postValue(e.message)
                    nearbyPlacesData.postValue(mutableListOf())
                }

                override fun onComplete() {
                    /* Nothing to do in here */
                }
            })
        compositeDisposable.add(disposableRefreshData)

    }

    fun onUpdateItemFavorite(adapterPosition: Int) {
        val nearbyPlacesDataTemp = nearbyPlacesData.value as MutableList
        val itemNearbyPlace = nearbyPlacesDataTemp[adapterPosition]
        itemNearbyPlace.isFavorite = !itemNearbyPlace.isFavorite
        val favoritePlaceEntity = FavoritePlaceEntity(
            itemNearbyPlace.id,
            itemNearbyPlace.idPlace,
            itemNearbyPlace.latitude,
            itemNearbyPlace.longitude,
            itemNearbyPlace.name,
            itemNearbyPlace.reference,
            itemNearbyPlace.type,
            itemNearbyPlace.photoCover,
            itemNearbyPlace.rating,
            itemNearbyPlace.userRatingTotal,
            itemNearbyPlace.address,
            itemNearbyPlace.distance,
            itemNearbyPlace.duration
        )
        when (itemNearbyPlace.isFavorite) {
            true -> {
                val disposableUpdateItemFavorite = Completable
                    .fromAction {
                        favoritePlaceRepository.insertFavoritePlace(favoritePlaceEntity)
                    }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CompletableObserver {
                        override fun onSubscribe(d: Disposable) {
                            compositeDisposable.add(d)
                        }

                        override fun onComplete() {
                            nearbyPlacesDataTemp[adapterPosition] = itemNearbyPlace
                            nearbyPlacesData.postValue(nearbyPlacesDataTemp)
                            messageItem.postValue(context.getString(R.string.message_favorite_added, itemNearbyPlace.name))
                        }

                        override fun onError(e: Throwable) {
                            e.printStackTrace()
                            messageItem.postValue(e.message!!)
                        }
                    })
            }
            else -> {
                val disposableUpdateItemFavorite = Completable
                    .fromAction {
                        favoritePlaceRepository.deleteFavoritePlaceById(favoritePlaceEntity.id)
                    }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CompletableObserver {
                        override fun onSubscribe(d: Disposable) {
                            compositeDisposable.add(d)
                        }

                        override fun onComplete() {
                            nearbyPlacesDataTemp[adapterPosition] = itemNearbyPlace
                            nearbyPlacesData.postValue(nearbyPlacesDataTemp)
                            messageItem.postValue(context.getString(R.string.message_favorite_removed, itemNearbyPlace.name))
                        }

                        override fun onError(e: Throwable) {
                            e.printStackTrace()
                            messageItem.postValue(e.message!!)
                        }
                    })
            }
        }
    }

}