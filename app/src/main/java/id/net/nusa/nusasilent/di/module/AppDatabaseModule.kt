package id.net.nusa.nusasilent.di.module

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import id.net.nusa.nusasilent.storage.db.AppDatabase
import id.net.nusa.nusasilent.storage.db.dao.favoriteplace.FavoritePlaceDao
import javax.inject.Singleton

@Module
class AppDatabaseModule {

    @Provides
    @Singleton
    fun providesAppDatabase(application: Application): AppDatabase =
            Room.databaseBuilder(application, AppDatabase::class.java, "NusaSilent_db")
                .fallbackToDestructiveMigration()
                .build()

    @Provides
    @Singleton
    fun providesFavoritePlaceDao(appDatabase: AppDatabase): FavoritePlaceDao = appDatabase.favoritePlaceDao()
}