package id.net.nusa.nusasilent.views.ui.fragments.dashboardfavorite

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.storage.db.entity.favoriteplace.FavoritePlaceEntity
import id.net.nusa.nusasilent.storage.db.repository.favoriteplace.FavoritePlaceRepository
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DashboardFavoriteViewModel @Inject constructor(
    private val context: Application,
    private val favoritePlaceRepository: FavoritePlaceRepository
) : AndroidViewModel(context) {

    private val compositeDisposable = CompositeDisposable()
    val loading = MutableLiveData<Boolean>()
    val error = MutableLiveData<String>()
    val messageItem = MutableLiveData<String>()
    val favoritePlacesData = MutableLiveData<List<FavoritePlaceEntity>>()

    fun onRefreshData() {
        loading.value = true
        error.value = ""
        val disposableRefreshData = favoritePlaceRepository.getAllFavoritePlaces()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<List<FavoritePlaceEntity>>() {
                override fun onNext(t: List<FavoritePlaceEntity>) {
                    loading.postValue(false)
                    error.postValue("")
                    favoritePlacesData.postValue(t)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    loading.postValue(false)
                    error.postValue(e.message)
                    favoritePlacesData.postValue(mutableListOf())
                }

                override fun onComplete() {
                    /* Nothing to do in here */
                }
            })
        compositeDisposable.add(disposableRefreshData)
    }

    fun onLoadData() {
        loading.value = true
        error.value = ""
        val disposableLoadData = favoritePlaceRepository.getAllFavoritePlaces()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<List<FavoritePlaceEntity>>() {
                override fun onNext(t: List<FavoritePlaceEntity>) {
                    loading.postValue(false)
                    error.postValue("")
                    favoritePlacesData.postValue(t)
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    loading.postValue(false)
                    error.postValue(e.message)
                    favoritePlacesData.postValue(mutableListOf())
                }

                override fun onComplete() {
                    /* Nothing to do in here */
                }
            })
        compositeDisposable.add(disposableLoadData)
    }

    fun onRemoveItemFavorite(adapterPosition: Int) {
        val favoritePlacesDataTemp = favoritePlacesData.value as MutableList
        val itemFavoritePlaceTemp = favoritePlacesDataTemp[adapterPosition]
        val disposableRemoveItemFavorite = Completable
            .fromAction {
                favoritePlaceRepository.deleteFavoritePlaceById(itemFavoritePlaceTemp.id)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onComplete() {
                    favoritePlacesDataTemp.removeAt(adapterPosition)
                    favoritePlacesData.value = favoritePlacesDataTemp
                    messageItem.postValue(
                        context.getString(
                            R.string.message_favorite_removed,
                            itemFavoritePlaceTemp.name
                        )
                    )
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                    messageItem.postValue(e.message)
                }
            })
    }

    fun dispose() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

}