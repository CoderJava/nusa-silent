package id.net.nusa.nusasilent.views.ui.fragments.dashboardmap

import android.annotation.SuppressLint
import android.media.AudioManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.di.injectable.Injectable
import id.net.nusa.nusasilent.model.nearbyplaces.PlaceData
import id.net.nusa.nusasilent.utils.*
import id.net.nusa.nusasilent.views.services.recognitionactivity.RecognitionActivitiesIntentService
import id.net.nusa.nusasilent.views.services.workmanager.LocationOfflineWorkManager
import kotlinx.android.synthetic.main.fragment_dashboard_map.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import views.services.workmanager.LocationWorkManager
import javax.inject.Inject

class DashboardMapFragment : Fragment(), Injectable, OnMapReadyCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var rootView: View
    private lateinit var googleMap: GoogleMap
    private lateinit var currentLocationCameraPosition: CameraPosition
    private lateinit var dashboardMapViewModel: DashboardMapViewModel

    companion object {
        fun newInstance(): DashboardMapFragment = DashboardMapFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_dashboard_map, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doRequestRuntimePermissionLocation()
        observeViewModel()
        registerEventBus()
    }

    private fun registerEventBus() {
        EventBus.getDefault().register(this)
    }

    private fun unregisterEventBus() {
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onMessageEvent(postData: HashMap<String, Any>) {
        when (postData["fromClass"].toString()) {
            RecognitionActivitiesIntentService::class.java.simpleName -> {
                dashboardMapViewModel.onGetLatestActivityRecognition()
            }
            LocationWorkManager::class.java.simpleName, LocationOfflineWorkManager::class.java.simpleName -> {
                dashboardMapViewModel.onGetRingerMode()
            }
        }
    }

    override fun onDestroy() {
        unregisterEventBus()
        dashboardMapViewModel.dispose()
        super.onDestroy()
    }

    private fun observeViewModel() {
        dashboardMapViewModel = ViewModelProviders.of(this, viewModelFactory).get(DashboardMapViewModel::class.java)
        dashboardMapViewModel.error.observe(this, Observer<String> {
            if (it.isNotEmpty()) {
                showLongSnackbar(rootView, it)
            }
        })
        dashboardMapViewModel.loading.observe(this, Observer<Boolean> {
            if (it) {
                loading_progress_bar_fragment_dashboard_map.visible()
            } else {
                loading_progress_bar_fragment_dashboard_map.gone()
            }
        })
        dashboardMapViewModel.nearbyPlaces.observe(this, Observer<List<PlaceData>> {
            googleMap.clear()
            it.forEach { placeData ->
                googleMap.addMarker(
                    MarkerOptions()
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                        .position(LatLng(placeData.latitude, placeData.longitude))
                        .title(placeData.name)
                        .snippet(
                            getString(R.string.rating) + ": " + getString(
                                R.string._d_of_d_users,
                                placeData.rating,
                                placeData.userRatingTotal
                            )
                        )
                )
            }
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(currentLocationCameraPosition))
        })
        dashboardMapViewModel.activityRecognition.observe(this, Observer<String> {
            text_view_activity_fragment_dashboard_map.text = it
        })
        dashboardMapViewModel.ringerMode.observe(this, Observer<Int> {
            when (it) {
                AudioManager.RINGER_MODE_NORMAL -> {
                    text_view_ringer_mode_fragment_dashboard_map.text = getString(R.string.normal)
                    text_view_ringer_mode_fragment_dashboard_map.setTextColor(
                        ContextCompat.getColor(
                            context!!,
                            R.color.colorPrimaryDark
                        )
                    )
                }
                AudioManager.RINGER_MODE_SILENT, AudioManager.RINGER_MODE_VIBRATE -> {
                    text_view_ringer_mode_fragment_dashboard_map.text = getString(R.string.silent)
                    text_view_ringer_mode_fragment_dashboard_map.setTextColor(
                        ContextCompat.getColor(
                            context!!,
                            R.color.colorGray600
                        )
                    )
                }
            }
        })
        dashboardMapViewModel.onGetLatestActivityRecognition()
        dashboardMapViewModel.onGetRingerMode()
    }

    private fun doRequestRuntimePermissionLocation() {
        requestRuntimePermission(
            activity!!,
            permissionLocation,
            this::actionPermissionLocationGranted,
            this::actionPermissionLocationDenied,
            this::actionPermissionLocationDeniedPermanent
        )
    }

    private fun actionPermissionLocationGranted() {
        initGoogleMaps()
    }

    private fun actionPermissionLocationDenied() {
        val alertDialogPermissionDenied = AlertDialog.Builder(context!!)
            .setTitle(getString(R.string.permission))
            .setMessage(getString(R.string.message_denied_location_permission))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.request_again)) { dialog, _ ->
                dialog.dismiss()
                doRequestRuntimePermissionLocation()
            }
            .setNegativeButton(getString(R.string.exit)) { dialog, _ ->
                dialog.dismiss()
                activity?.finish()
            }
            .create()
        alertDialogPermissionDenied.show()
    }

    private fun actionPermissionLocationDeniedPermanent() {
        showAlertDialogPermissionDeniedPermanent(activity!!, getString(R.string.message_denied_permanent))
    }

    private fun initGoogleMaps() {
        val mapFragment: SupportMapFragment? =
            childFragmentManager.findFragmentById(R.id.map_fragment_dashboard_map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap ?: return
        this.googleMap = googleMap
        with(googleMap) {
            isMyLocationEnabled = true
            doGetCurrentLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun doGetCurrentLocation() {
        val client = LocationServices.getFusedLocationProviderClient(context!!)
        client.lastLocation
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val currentLocation = it.result
                    val latitudeCurrentLocation = currentLocation?.latitude
                    val longitudeCurrentLocation = currentLocation?.longitude
                    val latLngCurrentLocation = LatLng(latitudeCurrentLocation!!, longitudeCurrentLocation!!)
                    currentLocationCameraPosition = CameraPosition.Builder()
                        .target(latLngCurrentLocation)
                        .zoom(15.5F)
                        .bearing(300f)
                        .tilt(50f)
                        .build()
                    doGetNearbyPlaces(latLngCurrentLocation)
                } else {
                    showLongSnackbar(rootView, it.exception?.message!!)
                }
            }
    }

    private fun doGetNearbyPlaces(latLngCurrentLocation: LatLng) {
        dashboardMapViewModel.onGetNearbyPlaces(latLngCurrentLocation)
    }

}
