package id.net.nusa.nusasilent.di.module

import dagger.Module
import dagger.Provides
import id.net.nusa.nusasilent.api.MainApi
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    fun providesMainApi(retrofit: Retrofit): MainApi = retrofit.create(MainApi::class.java)

}