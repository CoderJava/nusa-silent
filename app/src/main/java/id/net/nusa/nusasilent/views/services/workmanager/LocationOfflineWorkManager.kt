package id.net.nusa.nusasilent.views.services.workmanager

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioManager
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.room.Room
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.android.gms.location.LocationServices
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.storage.db.AppDatabase
import id.net.nusa.nusasilent.storage.db.entity.favoriteplace.FavoritePlaceEntity
import id.net.nusa.nusasilent.storage.db.repository.favoriteplace.FavoritePlaceRepository
import id.net.nusa.nusasilent.storage.sharedpreferences.SharedPreferencesManager
import id.net.nusa.nusasilent.utils.*
import id.net.nusa.nusasilent.views.ui.activities.dashboard.DashboardActivity
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class LocationOfflineWorkManager constructor(private val context: Context, workerParam: WorkerParameters) :
    Worker(context, workerParam) {

    override fun doWork(): Result {
        AnkoLogger<LocationOfflineWorkManager>().info { "doWork" }
        val checkPermissionLocation = ContextCompat.checkSelfPermission(context, permissionLocation)
        if (checkPermissionLocation == PackageManager.PERMISSION_GRANTED) {
            val client = LocationServices.getFusedLocationProviderClient(context)
            client.lastLocation
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val currentLocation = it.result
                        val latitudeCurrentLocation = currentLocation?.latitude
                        val longitudeCurrentLocation = currentLocation?.longitude
                        AnkoLogger<LocationOfflineWorkManager>().info { "current location: $latitudeCurrentLocation/$longitudeCurrentLocation" }
                        val appDatabase = Room.databaseBuilder(context, AppDatabase::class.java, "NusaSilent_db")
                            .fallbackToDestructiveMigration()
                            .build()
                        val favoritePlaceDao = appDatabase.favoritePlaceDao()
                        val favoritePlaceRepository = FavoritePlaceRepository(favoritePlaceDao)
                        favoritePlaceRepository.getAllFavoritePlaces()
                            .subscribeOn(Schedulers.io())
                            .observeOn(Schedulers.io())
                            .subscribeWith(object : DisposableObserver<List<FavoritePlaceEntity>>() {
                                override fun onNext(t: List<FavoritePlaceEntity>) {
                                    var isSwitchToSilentMode = false
                                    val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
                                    val currentRingerMode = audioManager.ringerMode
                                    val sharedPreferences =
                                        context.getSharedPreferences("PREF_DATA", Context.MODE_PRIVATE)
                                    val sharedPreferencesManager = SharedPreferencesManager(sharedPreferences)
                                    val isOnMosque =
                                        sharedPreferencesManager.getDataBoolean(SharedPreferencesManager.isOnMosque)
                                    for (favoritePlaceEntity in t) {
                                        val distanceInKm = calculateDistanceLocation(
                                            favoritePlaceEntity.latitude,
                                            favoritePlaceEntity.longitude,
                                            latitudeCurrentLocation!!,
                                            longitudeCurrentLocation!!
                                        )
                                        val distanceInMeter = distanceInKm * 1000
                                        AnkoLogger<LocationOfflineWorkManager>().info { "distanceInMeter: $distanceInMeter" }
                                        isSwitchToSilentMode = distanceInMeter > 0 && distanceInMeter <= 10
                                        if (isSwitchToSilentMode) {
                                            break
                                        }
                                    }
                                    if (audioManager.ringerMode != AudioManager.RINGER_MODE_SILENT) {
                                        if (isSwitchToSilentMode && currentRingerMode == AudioManager.RINGER_MODE_NORMAL) {
                                            audioManager.ringerMode = AudioManager.RINGER_MODE_VIBRATE
                                            sharedPreferencesManager.putDataBoolean(
                                                SharedPreferencesManager.isOnMosque,
                                                true
                                            )
                                            updateNotification(context)
                                        } else if (!isSwitchToSilentMode && isOnMosque) {
                                            audioManager.ringerMode = AudioManager.RINGER_MODE_NORMAL
                                            sharedPreferencesManager.putDataBoolean(
                                                SharedPreferencesManager.isOnMosque,
                                                false
                                            )
                                            updateNotification(context)
                                        }
                                    }
                                }

                                override fun onError(e: Throwable) {
                                    e.printStackTrace()
                                }

                                override fun onComplete() {
                                    /* Nothing to do in here */
                                }
                            })
                    }
                }
        }
        return Result.success()
    }

    private fun updateNotification(context: Context) {
        val titleNotification = "Nusa Silent"
        var contentNotification = context.getString(R.string.ringer) + ": "
        val ringerMode = getRingerModeDefault(context)
        contentNotification += when (ringerMode) {
            AudioManager.RINGER_MODE_NORMAL -> {
                context.getString(R.string.normal)
            }
            AudioManager.RINGER_MODE_SILENT, AudioManager.RINGER_MODE_VIBRATE -> {
                context.getString(R.string.silent)
            }
            else -> context.getString(R.string.unknown)
        }
        val alarmNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intentDashboardActivity = Intent(context, DashboardActivity::class.java)
        val pendingIntentNotification =
            PendingIntent.getActivity(context, 0, intentDashboardActivity, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(notificationChannelId, notificationChannelName, importance)
            alarmNotificationManager.createNotificationChannel(mChannel)
        }

        val alarmNotificationBuilder = NotificationCompat.Builder(context, notificationChannelId)
        alarmNotificationBuilder.setContentTitle(titleNotification)
        alarmNotificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
        alarmNotificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        alarmNotificationBuilder.setContentText(contentNotification)
        alarmNotificationBuilder.setAutoCancel(true)
        alarmNotificationBuilder.setContentIntent(pendingIntentNotification)
        alarmNotificationBuilder.setOngoing(true)
        alarmNotificationManager.notify(1, alarmNotificationBuilder.build())
        val postData = HashMap<String, Any>()
        postData["fromClass"] = javaClass.simpleName
        EventBus.getDefault().post(postData)

    }

}