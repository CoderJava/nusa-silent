package id.net.nusa.nusasilent.storage.db

import androidx.room.Database
import androidx.room.RoomDatabase
import id.net.nusa.nusasilent.storage.db.dao.favoriteplace.FavoritePlaceDao
import id.net.nusa.nusasilent.storage.db.entity.favoriteplace.FavoritePlaceEntity

@Database(entities = [(FavoritePlaceEntity::class)], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun favoritePlaceDao(): FavoritePlaceDao

}