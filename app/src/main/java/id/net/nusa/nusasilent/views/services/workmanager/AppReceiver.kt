package id.net.nusa.nusasilent.views.services.workmanager

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import id.net.nusa.nusasilent.utils.isNetworkAvailable
import views.services.workmanager.LocationWorkManager
import java.util.*

class AppReceiver : BroadcastReceiver() {

    private lateinit var pendingIntent: PendingIntent
    private val alarmRequestCode = 134
    private val intervalSeconds = 60 * 2

    override fun onReceive(context: Context?, intent: Intent?) {
        val alarmIntent = Intent(context, AppReceiver::class.java)
        pendingIntent = PendingIntent.getBroadcast(context, alarmRequestCode, alarmIntent, 0)
        val cal = Calendar.getInstance()
        cal.add(Calendar.SECOND, intervalSeconds)

        val manager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        if (Build.VERSION.SDK_INT >= 23) {
            manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.timeInMillis, pendingIntent)
        } else if (Build.VERSION.SDK_INT >= 19) {
            manager.setExact(AlarmManager.RTC_WAKEUP, cal.timeInMillis, pendingIntent)
        } else {
            manager.set(AlarmManager.RTC_WAKEUP, cal.timeInMillis, pendingIntent)
        }
        if (!isNetworkAvailable(context)) {
            val locationOfflineWorkManager = OneTimeWorkRequest.Builder(LocationOfflineWorkManager::class.java)
                .build()
            WorkManager.getInstance(context).enqueue(locationOfflineWorkManager)
        } else {
            val constraintsLocationWorkManager = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()
            val locationWorkManager = OneTimeWorkRequest.Builder(LocationWorkManager::class.java)
                .setConstraints(constraintsLocationWorkManager)
                .build()
            WorkManager.getInstance(context).enqueue(locationWorkManager)
        }
    }

}