package id.net.nusa.nusasilent.views.services.boot

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.utils.getRingerModeDefault
import id.net.nusa.nusasilent.utils.notificationChannelId
import id.net.nusa.nusasilent.utils.notificationChannelName
import id.net.nusa.nusasilent.views.services.workmanager.AppReceiver
import id.net.nusa.nusasilent.views.ui.activities.dashboard.DashboardActivity
import views.services.workmanager.LocationWorkManager
import java.util.*

class StartupBootReceiver : BroadcastReceiver() {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context?, intent: Intent?) {
        val alarmIntent = Intent(context, AppReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(context, 134, alarmIntent, 0)
        val cal = Calendar.getInstance()
        cal.add(Calendar.SECOND, 10)
        val manager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        manager.set(AlarmManager.RTC_WAKEUP, cal.timeInMillis, pendingIntent)
        createNotification(context)
    }

    private fun createNotification(context: Context) {
        val titleNotification = "Nusa Silent"
        var contentNotification = context.getString(R.string.ringer) + ": "
        val ringerMode = getRingerModeDefault(context)
        contentNotification += when (ringerMode) {
            AudioManager.RINGER_MODE_NORMAL -> {
                context.getString(R.string.normal)
            }
            AudioManager.RINGER_MODE_SILENT, AudioManager.RINGER_MODE_VIBRATE -> {
                context.getString(R.string.silent)
            }
            else -> context.getString(R.string.unknown)
        }
        val alarmNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intentDashboardActivity = Intent(context, DashboardActivity::class.java)
        val pendingIntentNotification =
            PendingIntent.getActivity(context, 0, intentDashboardActivity, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(notificationChannelId, notificationChannelName, importance)
            alarmNotificationManager.createNotificationChannel(mChannel)
        }

        val alarmNotificationBuilder = NotificationCompat.Builder(context, notificationChannelId)
        alarmNotificationBuilder.setContentTitle(titleNotification)
        alarmNotificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
        alarmNotificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        alarmNotificationBuilder.setContentText(contentNotification)
        alarmNotificationBuilder.setAutoCancel(true)
        alarmNotificationBuilder.setContentIntent(pendingIntentNotification)
        alarmNotificationBuilder.setOngoing(true)
        alarmNotificationManager.notify(1, alarmNotificationBuilder.build())
        val locationWorkManager = OneTimeWorkRequest.Builder(LocationWorkManager::class.java)
            .build()
        WorkManager.getInstance(context).enqueue(locationWorkManager)
    }

}