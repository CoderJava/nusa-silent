package id.net.nusa.nusasilent.views.ui.fragments.dashboardfavorite


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import id.net.nusa.nusasilent.R
import id.net.nusa.nusasilent.di.injectable.Injectable
import id.net.nusa.nusasilent.model.nearbyplaces.PlaceData
import id.net.nusa.nusasilent.storage.db.entity.favoriteplace.FavoritePlaceEntity
import id.net.nusa.nusasilent.utils.*
import id.net.nusa.nusasilent.views.ui.fragments.dashboardfavorite.adapter.AdapterFavoritePlace
import kotlinx.android.synthetic.main.fragment_dashboard_favorite.view.*
import javax.inject.Inject

class DashboardFavoriteFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var rootView: View
    private lateinit var dashboardFavoriteViewModel: DashboardFavoriteViewModel
    private lateinit var placesData: MutableList<PlaceData>
    private lateinit var adapterFavoritePlace: AdapterFavoritePlace
    private lateinit var placeDataSelected: PlaceData

    companion object {
        fun newInstance(): DashboardFavoriteFragment = DashboardFavoriteFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_dashboard_favorite, container, false)
        initAdapterFavoritePlace()
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
        observeViewModel()
        doLoadData()
    }

    override fun onDestroy() {
        dashboardFavoriteViewModel.dispose()
        super.onDestroy()
    }

    private fun initAdapterFavoritePlace() {
        placesData = mutableListOf()
        adapterFavoritePlace =
            AdapterFavoritePlace(context!!, placesData, object : AdapterFavoritePlace.ListenerAdapterFavoritePlace {
                override fun onClickRemoveItemFavorite(adapterPosition: Int) {
                    val alertDialogInfo = AlertDialog.Builder(context!!)
                        .setTitle(getString(R.string.confirmation))
                        .setCancelable(false)
                        .setMessage(getString(R.string.are_you_sure_want_to_remove_this_place_from_your_favorite_place))
                        .setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                            dialog.dismiss()
                            dashboardFavoriteViewModel.onRemoveItemFavorite(adapterPosition)
                        }
                        .setNegativeButton(getString(R.string.no)) { dialog, _ ->
                            dialog.dismiss()
                        }
                        .create()
                    alertDialogInfo.show()
                }

                @SuppressLint("MissingPermission")
                override fun onClickItemDirection(placeData: PlaceData) {
                    placeDataSelected = placeData
                    doRequestRuntimePermissionLocation()
                }
            })
    }

    private fun doRequestRuntimePermissionLocation() {
        requestRuntimePermission(
            activity!!,
            permissionLocation,
            this::actionPermissionLocationGranted,
            this::actionPermissionLocationDenied,
            this::actionPermissionLocationDeniedPermanent
        )
    }

    @SuppressLint("MissingPermission")
    private fun actionPermissionLocationGranted() {
        val client = LocationServices.getFusedLocationProviderClient(context!!)
        client.lastLocation
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val currentLocation = it.result
                    val latitudeCurrentLocation = currentLocation?.latitude
                    val longitudeCurrentLocation = currentLocation?.longitude
                    val latLngCurrentLocation =
                        LatLng(latitudeCurrentLocation!!, longitudeCurrentLocation!!)
                    val strLatLngCurrentLocation =
                        "${latLngCurrentLocation.latitude},${latLngCurrentLocation.longitude}"
                    val strLatLngDesintationLocation = "${placeDataSelected.latitude},${placeDataSelected.longitude}"
                    intentGoogleMapsDirection(
                        context!!,
                        strLatLngCurrentLocation,
                        strLatLngDesintationLocation
                    )
                }
            }
    }

    private fun actionPermissionLocationDenied() {
        val alertDialogInfoPermissionDenied = AlertDialog.Builder(context!!)
            .setTitle(getString(R.string.permission))
            .setMessage(getString(R.string.message_denied_location_permission))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.request_again)) { dialog, _ ->
                dialog.dismiss()
                doRequestRuntimePermissionLocation()
            }
            .setNegativeButton(getString(R.string.exit)) { dialog, _ ->
                dialog.dismiss()
                activity?.finish()
            }
            .create()
        alertDialogInfoPermissionDenied.show()
    }

    private fun actionPermissionLocationDeniedPermanent() {
        showAlertDialogPermissionDeniedPermanent(activity!!, getString(R.string.message_denied_permanent))
    }

    fun observeViewModel() {
        dashboardFavoriteViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(DashboardFavoriteViewModel::class.java)
        dashboardFavoriteViewModel.error.observe(this, Observer<String> {
            if (it.isEmpty()) {
                rootView.recycler_view_fragment_dashboard_favorite.visible()
            } else {
                rootView.recycler_view_fragment_dashboard_favorite.gone()
                showLongSnackbar(rootView, it)
            }
        })
        dashboardFavoriteViewModel.messageItem.observe(this, Observer<String> {
            if (!it.isEmpty()) {
                showLongSnackbar(rootView, it)
            }
        })
        dashboardFavoriteViewModel.loading.observe(this, Observer<Boolean> {
            rootView.swipe_refresh_layout_fragment_dashboard_favorite.isRefreshing = it
        })
        dashboardFavoriteViewModel.favoritePlacesData.observe(this, Observer<List<FavoritePlaceEntity>> {
            placesData.clear()
            it.forEach {
                placesData.add(
                    PlaceData(
                        it.id,
                        it.idPlace,
                        it.latitude,
                        it.longitude,
                        it.name,
                        it.reference,
                        it.type,
                        it.photoCover,
                        true,
                        it.rating,
                        it.userRatingTotal,
                        it.address,
                        it.distance,
                        it.duration
                    )
                )
            }
            if (rootView.recycler_view_fragment_dashboard_favorite.adapter == null) {
                rootView.recycler_view_fragment_dashboard_favorite.layoutManager = LinearLayoutManager(context)
                rootView.recycler_view_fragment_dashboard_favorite.adapter = adapterFavoritePlace
            } else {
                adapterFavoritePlace.refresh(placesData)
            }
        })
    }

    private fun initListeners() {
        rootView.swipe_refresh_layout_fragment_dashboard_favorite.setOnRefreshListener {
            doRefreshData()
        }
    }

    private fun doLoadData() {
        dashboardFavoriteViewModel.onLoadData()
    }

    private fun doRefreshData() {
        dashboardFavoriteViewModel.onRefreshData()
    }

    private fun initViews() {
        setColorLoadingSwipeRefreshLayout(context!!, rootView.swipe_refresh_layout_fragment_dashboard_favorite)
    }

}
