package id.net.nusa.nusasilent.views.services.recognitionactivity

import android.app.IntentService
import android.content.Context
import android.content.Intent
import com.google.android.gms.location.ActivityRecognitionResult
import id.net.nusa.nusasilent.storage.sharedpreferences.SharedPreferencesManager
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class RecognitionActivitiesIntentService : IntentService("RecognitionActivity") {

    override fun onHandleIntent(intent: Intent?) {
        val result = ActivityRecognitionResult.extractResult(intent)
        val detectedActivities = result.probableActivities as ArrayList
        val sharedPreferences = getSharedPreferences("PREF_DATA", Context.MODE_PRIVATE)
        val sharedPreferencesManager = SharedPreferencesManager(sharedPreferences)
        sharedPreferencesManager.putDataString(
            ConstantsRecognitionActivity.keyDetectedActivities,
            UtilsRecognitionActivity.detectedActivitiesToJson(detectedActivities)
        )
        AnkoLogger<RecognitionActivitiesIntentService>().info { "activities detected" }
        var latestActivityRecognition = ""
        var confidenceLatestActivityRecognition = 0
        detectedActivities.forEach {
            val activityRecognition = UtilsRecognitionActivity.getActivityString(applicationContext, it.type)
            val confidenceActivityRecognition = it.confidence
            AnkoLogger<RecognitionActivitiesIntentService>().info {
                "$activityRecognition $confidenceActivityRecognition %"
            }
            if (confidenceLatestActivityRecognition < confidenceActivityRecognition) {
                latestActivityRecognition = activityRecognition
                confidenceLatestActivityRecognition = confidenceActivityRecognition
            }
        }
        AnkoLogger<RecognitionActivitiesIntentService>().info { "latestActivityRecognition: $latestActivityRecognition $confidenceLatestActivityRecognition" }
        sharedPreferencesManager.putDataString(SharedPreferencesManager.activityRecognition, latestActivityRecognition)
        val postData = HashMap<String, Any>()
        postData["fromClass"] = javaClass.simpleName
        EventBus.getDefault().post(postData)
    }
}