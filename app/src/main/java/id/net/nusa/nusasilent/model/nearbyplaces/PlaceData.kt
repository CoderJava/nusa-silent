package id.net.nusa.nusasilent.model.nearbyplaces

data class PlaceData (
    val id: String = "",
    val idPlace: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val name: String = "",
    val reference: String = "",
    val type: String = "",
    val photoCover: String = "",
    var isFavorite: Boolean = false,
    val rating: Double = 0.0,
    val userRatingTotal: Int = 0,
    var address: String = "",
    var distance: String = "",
    var duration: String = ""
)