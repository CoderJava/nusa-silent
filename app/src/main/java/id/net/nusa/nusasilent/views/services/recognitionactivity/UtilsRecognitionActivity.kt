package id.net.nusa.nusasilent.views.services.recognitionactivity

import android.content.Context
import com.google.android.gms.location.DetectedActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.net.nusa.nusasilent.R

class UtilsRecognitionActivity {

    companion object {
        fun getActivityString(context: Context, detectedActivityType: Int): String {
            val resources = context.resources
            return when (detectedActivityType) {
                DetectedActivity.IN_VEHICLE, DetectedActivity.ON_BICYCLE -> {
                    resources.getString(R.string.driving)
                }
                DetectedActivity.ON_FOOT, DetectedActivity.WALKING, DetectedActivity.RUNNING -> {
                    resources.getString(R.string.walking)
                }
                else -> {
                    resources.getString(R.string.still)
                }
            }
        }

        fun detectedActivitiesToJson(detectedActivitiesList: ArrayList<DetectedActivity>): String {
            val type = object : TypeToken<ArrayList<DetectedActivity>>() {}.type
            return Gson().toJson(detectedActivitiesList, type)
        }

        fun detectedActivitiesFromJson(jsonArray: String): ArrayList<DetectedActivity> {
            val listType = object : TypeToken<ArrayList<DetectedActivity>>(){}.type
            var detectedActivities = Gson().fromJson<ArrayList<DetectedActivity>>(jsonArray, listType)
            if (detectedActivities == null) {
                detectedActivities = ArrayList()
            }
            return detectedActivities
        }
    }

}