package id.net.nusa.nusasilent.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.net.nusa.nusasilent.views.ui.fragments.dashboardfavorite.DashboardFavoriteFragment
import id.net.nusa.nusasilent.views.ui.fragments.dashboardmap.DashboardMapFragment
import id.net.nusa.nusasilent.views.ui.fragments.dashboardplace.DashboardPlaceFragment


@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeDashboardPlaceFragment(): DashboardPlaceFragment

    @ContributesAndroidInjector
    abstract fun contributeDashboardMapFragment(): DashboardMapFragment

    @ContributesAndroidInjector
    abstract fun contributeDashboardFavoriteFragment(): DashboardFavoriteFragment

}